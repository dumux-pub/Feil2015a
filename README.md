## Summary

This is the DuMuX module containing the code for producing the results
published in:

Feil, Miriam

Untersuchungen zu Möglichkeiten der Wärmespeicherung bei der adiabaten Druckluftspeicherung

BSc-Thesis 2015


## Installation

The easiest way to install this module is to create a new folder and to execute the file
[installFeil2015a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Feil2015a/raw/master/installFeil2015a.sh)
in this folder.

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


## Applications

The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

- appl/bsc-Miriam/

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

- appl/bsc-Miriam/test_cc1pniTES.cc

have been extracted.


## Used Versions and Software

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installFeil2015a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Feil2015a/raw/master/installFeil2015a.sh).

No additional external software package are necessary for
compiling the executables.


## Installation with Docker

Create a new folder and change into it

```bash
mkdir Feil2015a
cd Feil2015a
```

Then download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/Feil2015a/-/raw/master/docker_feil2015a.sh
```

and open the docker container by running
```bash
bash docker_feil2015a.sh open
```

After the script has run successfully, executable can be built and run
```bash
cd Feil2015a/build-cmake/appl/bsc-Miriam
make test_cc1pniTES
./test_cc1pniTES test_cc1pniTES.input
```

