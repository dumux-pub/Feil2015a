// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief Test for the OnePModel in combination with the NI model for a convection problem:
 * The simulation domain is a tube where water with an elevated temperature is injected
 * at a constant rate on the left hand side.
 */
#ifndef DUMUX_1PNI_CONVECTION_PROBLEM_HH
#define DUMUX_1PNI_CONVECTION_PROBLEM_HH

#include <math.h>
// The DUNE grid used
#if HAVE_ALUGRID
#include <dune/grid/alugrid.hh>
#elif HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#elif HAVE_UG
#include <dune/grid/uggrid.hh>
#else
#include <dune/grid/yaspgrid.hh>
#endif // HAVE_ALUGRID, HAVE_UG

#include <dumux/io/cubegridcreator.hh>

#include <dumux/implicit/1p/1pmodel.hh>..
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/air.hh> //neu statt h2o
#include <dumux/material/fluidmatrixinteractions/1p/thermalconductivityaverage.hh>
#include <dumux/io/gnuplotinterface.hh>
#include "1pnispatialparams.hh"



namespace Dumux
{

template <class TypeTag>
class OnePNIConvectionProblem;

namespace Properties
{
NEW_TYPE_TAG(OnePNIConvectionProblem, INHERITS_FROM(OnePNI));
NEW_TYPE_TAG(OnePNIConvectionBoxProblem, INHERITS_FROM(BoxModel, OnePNIConvectionProblem));
NEW_TYPE_TAG(OnePNIConvectionCCProblem, INHERITS_FROM(CCModel, OnePNIConvectionProblem));

// Set grid and the grid creator to be used
#if HAVE_ALUGRID || HAVE_DUNE_ALUGRID /*@\label{tutorial-coupled:set-grid}@*/
SET_TYPE_PROP(OnePNIConvectionProblem, Grid, Dune::ALUGrid</*dim=*/2, 2, Dune::cube, Dune::nonconforming>); /*@\label{tutorial-coupled:set-grid-ALU}@*/
#elif HAVE_UG
SET_TYPE_PROP(OnePNIConvectionProblem, Grid, Dune::UGGrid<2>);
#else
SET_TYPE_PROP(OnePNIConvectionProblem, Grid, Dune::YaspGrid<2>);
#endif // HAVE_ALUGRID
SET_TYPE_PROP(OnePNIConvectionProblem, GridCreator, Dumux::CubeGridCreator<TypeTag>); /*@\label{tutorial-coupled:set-gridcreator}@*/

// Set the problem property
SET_TYPE_PROP(OnePNIConvectionProblem, Problem,
              Dumux::OnePNIConvectionProblem<TypeTag>);

SET_SCALAR_PROP(OnePNIConvectionProblem, ImplicitMassUpwindWeight, 1.0);

// Set the fluid system
SET_TYPE_PROP(OnePNIConvectionProblem,
              Fluid,
              Dumux::GasPhase<typename GET_PROP_TYPE(TypeTag, Scalar),
                                 Dumux::Air<typename GET_PROP_TYPE(TypeTag, Scalar)> >);


// Set the spatial parameters
SET_TYPE_PROP(OnePNIConvectionProblem,
              SpatialParams,
              Dumux::OnePNISpatialParams<TypeTag>);


}


/*!
 * \ingroup OnePNIModel
 * \ingroup ImplicitTestProblems
 *
 * \brief Test for the OnePModel in combination with the NI model for a convection problem:
 * The simulation domain is a 2D rectangular thermal energy storage where hot air is injected at the top.
 * At the bottom the air flows out and is stored in a cavern. (Adapted from the project ADELE)
 *
 * Initially the domain is fully saturated with air at a constant low temperature.
 * On the right and left side there Neumann boundaries for the pressure (No-flow) and
 * Neumann boundaries which simulate the losses because of heat transfer.
 * (Maybe the coefficient is too high!)
 * On the top there is a Dirichlet boundary with a constant temperature and a constant pressure.
 * On the bottom there is a Dirichlet boundary condition for the pressure (it leads to an exponential process
 * because of the loading state of the connected cavern).
 * For the temperature there is a Outflow condition.
 * After loading the thermal energy storage, the conditions were swapped and the cold air is injected from the
 * bottom to the top.
 *
 * There has to be used the Darcy-Forchheimer law instead of the Darcy-Law!
 *
 * The results are compared to an analytical solution where a retarded front velocity is calculated as follows:
  \f[
     v_{Front}=\frac{q S_{water}}{\phi S_{total}}
 \f]
 *
 * The result of the analytical solution is written into the vtu files.
 * This problem uses the \ref OnePModel and \ref NIModel model.
 *
 * To run the simulation execute the following line in shell: <br>
 * <tt>./test_box1pniconvection -ParameterFile ./test_box1pniconvection.input</tt> or <br>
 * <tt>./test_cc1pniconvection -ParameterFile ./test_cc1pniconvection.input</tt>
 */
template <class TypeTag>
class OnePNIConvectionProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, ThermalConductivityModel) ThermalConductivityModel;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;

    typedef Dumux::Air<Scalar> Air;


    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // world dimension
        dimWorld = GridView::dimensionworld
    };

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dimWorld : 0 };

    enum {
        // indices of the primary variables
        pressureIdx = Indices::pressureIdx,
        temperatureIdx = Indices::temperatureIdx
    };
    enum {
        // index of the transport equation
        conti0EqIdx = Indices::conti0EqIdx,
        energyEqIdx = Indices::energyEqIdx
    };


    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::Intersection Intersection;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    OnePNIConvectionProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
        , eps_(1e-6)
    {
        //initialize fluid system
        FluidSystem::init();

        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             std::string,
                                             Problem, Name);
        outputInterval_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                int, Problem, OutputInterval);
        darcyVelocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                Scalar, Problem, DarcyVelocity);

        temperatureHigh_ = 913.;
        temperatureLow_ = 333.;
        pressureHigh_ = 7e6;
        pressureLow_ = 5e6;
    }


    bool shouldWriteOutput() const
    {
        return
            this->timeManager().timeStepIndex() == 0 ||
            this->timeManager().timeStepIndex() % outputInterval_ == 0 ||
            this->timeManager().episodeWillBeOver() ||
            this->timeManager().willBeFinished();
    }

    /*!
     * \brief Append all quantities of interest which can be derived
     *        from the solution of the current time step to the VTK
     *        writer.
     */

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position for which the bc type should be evaluated
     */



    void boundaryTypesAtPos(BoundaryTypes &values,
                            const GlobalPosition &globalPos) const
    {
      const Scalar time = this->timeManager().time() + this->timeManager().timeStepSize();
        const Scalar time_end = this->timeManager().endTime();

      //temporal progress of boundary conditions

      if (time >=0 && time < 50) //set the initial conditions
      {
    values.setAllDirichlet();
      }


      else if (time >= 50 && time <= 2.885e4) //time for loading
      {
      if (globalPos[1] > this->bBoxMax()[1] - eps_)
      {
        values.setAllDirichlet();
      }
      else if (globalPos[1] < eps_)
      {
       values.setDirichlet(Indices::pressureIdx);
       values.setOutflow(energyEqIdx);
      }
      else
      {
        values.setAllNeumann();
      }
      }
      else if (time > 2.885e4 && time <= 3.2e4) //standstill time
      {
    values.setAllNeumann();
      }
      else //time for discharging
      {
      if (globalPos[1] > this->bBoxMax()[1] - eps_)
      {
        values.setDirichlet(pressureIdx);
        values.setOutflow(energyEqIdx);
      }
      else if (globalPos[1] < eps_)
      {
        values.setAllDirichlet();
      }
      else
      {
        values.setAllNeumann();
      }
      }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The position for which the bc type should be evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
      ElementVolumeVariables elemVolVars;
    const Scalar time = this->timeManager().time() + this->timeManager().timeStepSize();
        const Scalar time_end = this->timeManager().endTime();


    if (time >=0 && time < 50)
      {
    values[pressureIdx] = pressureLow_;
    values[temperatureIdx] = temperatureLow_;

      }



      else if (time >= 50 && time <= 2.885e4)
      {
    if (globalPos[1] > this->bBoxMax()[1] - eps_)
      {
    values[pressureIdx] = pressureHigh_;
    values[temperatureIdx] = temperatureHigh_;
      }
       else if (globalPos[1] < eps_)
        {
          values[pressureIdx] = pressureHigh_-(pressureHigh_-pressureLow_)*(pow(2.718,(-0.000055*(time-50))/1.0));
          values[temperatureIdx] = temperatureLow_;
        }
    else
    {
      values[pressureIdx] = 0;
      values[temperatureIdx] = 0;
    }

      }

      else
      {
    if (globalPos[1] > this->bBoxMax()[1] - eps_)
    {
        values[pressureIdx] = pressureLow_;
        values[temperatureIdx] = temperatureHigh_;
    }

    if (globalPos[1] < eps_)
    {
          values[temperatureIdx] = temperatureLow_;
          values[pressureIdx] = pressureLow_+(pressureHigh_-pressureLow_)*(pow(2.718,(-0.000078*(time-3.2e4))/1.0));


    }
      }


    }
    /*!
     * \brief Evaluates the boundary conditions for a Neumann
     *        boundary segment in dependency on the current solution.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param intersection The intersection between element and boundary
     * \param scvIdx The local index of the sub-control volume
     * \param boundaryFaceIdx The index of the boundary face
     * \param elemVolVars All volume variables for the element
     *
     * This method is used for cases, when the Neumann condition depends on the
     * solution and requires some quantities that are specific to the fully-implicit method.
     * The \a values store the mass flux of each phase normal to the boundary.
     * Negative values indicate an inflow.
     */

    void solDependentNeumann(PrimaryVariables &values,
                      const Element &element,
                      const FVElementGeometry &fvGeometry,
                      const Intersection &intersection,
                      const int scvIdx,
                      const int boundaryFaceIdx,
                      const ElementVolumeVariables &elemVolVars) const

    {
        values = 0;

    const Scalar time = this->timeManager().time() + this->timeManager().timeStepSize();
        const Scalar time_end = this->timeManager().endTime();
    GlobalPosition globalPos(0);

    //losses because of heat transfer are calculate: -\frac{1}{\frac{1}{\alpha_1}+\frac{1}{\alpha_2}+\frac{d}{\lambda}}\cdot \Delta\theta
    //where alpha is the heat transfer coefficient from the fluid to the wall of the thermal energy storage, d the thickness of the storage
    //and lambda the heat conductivity coefficient.

      if (time >=50 && time <= 2.885e4)
      {
          values[temperatureIdx] = -1/((1/50)+(1/70)+(0.1/0.035))*(273.15 - elemVolVars[0].temperature()); //losses because of heat transfer
          values[pressureIdx] = 0;
        }
    else if (time > 2.885e4 && time <= 3.2e4)
    {
          if (globalPos[0] < eps_)
        {
          values[temperatureIdx] = -1/((1/50)+(1/70)+(0.1/0.035))*(273.15 - elemVolVars[0].temperature()); //losses because of heat transfer
          values[pressureIdx] = 0;
        }

        else
        {
          values[temperatureIdx] = -1/((1/50)+(1/70)+(0.1/0.035))*(273.15 - elemVolVars[0].temperature());//losses because of heat transfer
          values[pressureIdx] = 0;
        }
    }

    else if (time > 3.2e4)
    {
          values[temperatureIdx] = -1/((1/50)+(1/70)+(0.1/0.035))*(273.15 - elemVolVars[0].temperature()); //losses because of heat transfer
          values[pressureIdx] = 0;
    }

    else
    {
      values[temperatureIdx] = 0;
      values[pressureIdx] = 0;
    }
    }



    /*!
     * \name Volume terms
     */

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a priVars parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void sourceAtPos(PrimaryVariables &priVars,
                     const GlobalPosition &globalPos) const
    {
        priVars = Scalar(0.0);
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }


//write out data and plot with Gnuplot
   void postTimeStep()
    {
        ParentType::postTimeStep();
        Scalar inflow(0.0);
    Scalar inflow_sum(0.0);
        Scalar outflow(0.0);
    Scalar outflow_sum(0.0);
        Scalar simulationTime = this->timeManager().time();
        Scalar temperature;
    Scalar temperature_top;
    Scalar temperature_middle;
    Scalar temperature_middleB;
        Scalar pressure_t;
    Scalar pressure_b;
    Scalar energy(0.0);
    Scalar energy_b(0.0);
    Scalar gasEnthalpy;
    Scalar losses_r(0.0);
    Scalar losses_l(0.0);
    Scalar losses_b(0.0);
    Scalar losses_t(0.0);
        energy = 0.0;

              // Loop over all elements
        for (ElementIterator element = this->gridView().template begin<0>(); element != this->gridView().template end<0>(); ++element)
        {
            const auto geometry = element->geometry();

            FVElementGeometry fvGeometry;
            fvGeometry.update(this->gridView(),*element);

            ElementVolumeVariables elemVolVars;
            elemVolVars.update(*this,
                               *element,
                                fvGeometry,
                                false /* oldSol? */);

            // loop over all intersections
            for (IntersectionIterator is = this->gridView().ibegin(*element); is != this->gridView().iend(*element); ++is)
            {
        if(is->boundary())
                {
                    int fIdx = is->indexInInside();


                        const Scalar x = is->geometry().center()[0];
                        const Scalar y = is->geometry().center()[1];
                        if (y < eps_)
                        {
                          pressure_b = elemVolVars[0].pressure();
              // svcf on the bottom
                            FluxVariables fluxVars;
			    fluxVars.update(*this, *element, fvGeometry, fIdx, elemVolVars, true);
                            outflow_sum += fluxVars.volumeFlux(0) * elemVolVars[fluxVars.upstreamIdx(0)].density();
                outflow = fluxVars.volumeFlux(0) * elemVolVars[fluxVars.upstreamIdx(0)].density();


                 if (x < this->bBoxMax()[0]/GET_RUNTIME_PARAM(TypeTag, Scalar, Grid.NumberOfCellsX)/2.0 + eps_)
                 {
                  temperature = elemVolVars[0].temperature();

                 }

             energy_b += outflow * -1.0 * Air::gasEnthalpy(elemVolVars[fluxVars.upstreamIdx(0)].temperature(), elemVolVars[fluxVars.upstreamIdx(0)].pressure()) * this->timeManager().previousTimeStepSize();

             if (x < this->bBoxMax()[0]/2.0 + eps_ && x < this->bBoxMax()[0]/2.0 - eps_)
              {
                temperature_middleB = elemVolVars[0].temperature();
              }
                        }
                        if (y > this->bBoxMax()[1] - eps_)
                        {
                            // scvf on the top
                            FluxVariables fluxVars;
			    fluxVars.update(*this, *element, fvGeometry, fIdx, elemVolVars, true);
                            inflow_sum += fluxVars.volumeFlux(0)*elemVolVars[fluxVars.upstreamIdx(0)].density();
                inflow = fluxVars.volumeFlux(0)*elemVolVars[fluxVars.upstreamIdx(0)].density();

                                pressure_t = elemVolVars[0].pressure();

            if (elemVolVars[fluxVars.upstreamIdx(0)].temperature() > 333.3333)
            {
                energy += inflow * -1.0 * Air::gasEnthalpy(elemVolVars[fluxVars.upstreamIdx(0)].temperature(), elemVolVars[fluxVars.upstreamIdx(0)].pressure()) * this->timeManager().previousTimeStepSize();

            }

              if (x < this->bBoxMax()[0]/GET_RUNTIME_PARAM(TypeTag, Scalar, Grid.NumberOfCellsX)/2.0 + eps_)
              {
              temperature_top = elemVolVars[0].temperature();
              }
              if (x < this->bBoxMax()[0]/2.0 + eps_ && x < this->bBoxMax()[0]/2.0 - eps_)
              {
                temperature_middle = elemVolVars[0].temperature();
              }


            }
            if (x > this->bBoxMax()[0] - eps_)
            {
              losses_r += -1/((1/50)+(1/70)+(0.1/0.035))*(273.15 - elemVolVars[0].temperature());
            }
            if (x <  eps_)
            {
              losses_l += -1/((1/50)+(1/70)+(0.1/0.035))*(273.15 - elemVolVars[0].temperature());
            }
            if (y < eps_)
            {
              losses_b += -1/((1/50)+(1/70)+(0.1/0.035))*(273.15 - elemVolVars[0].temperature());
            }
            if (y > this->bBoxMax()[1] - eps_)
            {
              losses_t += -1/((1/50)+(1/70)+(0.1/0.035))*(273.15 - elemVolVars[0].temperature());
            }
                }
            }



        }

      // Gnuplot
      // variables that don't go out of scope
      static double yMax = -1e100;
      static double y2Max = -1e100;
      static std::vector<double> x;
      static std::vector<double> y;
      static std::vector<double> y2;
      static std::vector<double> y3;
      static std::vector<double> y4;
      static std::vector<double> y5;
      static std::vector<double> y6;
      static std::vector<double> y7;
      static std::vector<double> y8;
      static std::vector<double> y9;
      static std::vector<double> y10;
      static std::vector<double> y11;
      static std::vector<double> y12;
      static std::vector<double> y13;
      static std::vector<double> y14;
      static double accumulatedMass = 0.0;
      static double accumulatedEnergy = 0.0;
      static double accumulatedEnergy_b = 0.0;
      static double accumulatedLosses_s = 0.0;
      static double accumulatedLosses_t = 0.0;
      static double accumulatedLosses_b = 0.0;

      double inflow_plot = -1.0*inflow_sum*1000;
      double outflow_plot = -1.0*outflow_sum*1000;
      pressure_t = pressure_t*1e-5;
      pressure_b = pressure_b*1e-5;
      temperature = temperature - 273.15;
      temperature_middleB = temperature_middleB - 273.15;
      temperature_top = temperature_top - 273.15;
      temperature_middle = temperature_middle - 273.15;
      accumulatedMass += 1e-6*inflow_plot*this->timeManager().previousTimeStepSize();
      accumulatedEnergy += energy*1e-9;
      accumulatedEnergy_b += energy_b * 1e-9;
      accumulatedLosses_s += (losses_r + losses_l)*this->timeManager().previousTimeStepSize();
      accumulatedLosses_t += losses_t*this->timeManager().previousTimeStepSize();
      accumulatedLosses_b += losses_b*this->timeManager().previousTimeStepSize();


      x.push_back(this->timeManager().time());
      y.push_back(inflow_plot);
      y2.push_back(pressure_t);
      y3.push_back(accumulatedMass);
      y4.push_back(temperature);
      y5.push_back(outflow_plot);
      y6.push_back(accumulatedEnergy);
      y7.push_back(temperature_top);
      y8.push_back(temperature_middle);
      y9.push_back(accumulatedEnergy_b);
      y10.push_back(temperature_middleB);
      y11.push_back(pressure_b);
      y12.push_back(accumulatedLosses_s);
      y13.push_back(accumulatedLosses_t);
      y14.push_back(accumulatedLosses_b);


      yMax = std::max({yMax, inflow_plot, pressure_t, pressure_b, outflow_plot, accumulatedLosses_s, accumulatedLosses_b, accumulatedLosses_t});
      y2Max = std::max({y2Max, accumulatedMass, temperature, accumulatedEnergy, temperature_top, temperature_middle, accumulatedEnergy_b, temperature_middleB});

      gnuplot_.reset();
      gnuplot_.setXRange(0, x.back()+1);
      gnuplot_.setYRange(0, yMax+1);
      gnuplot_.setXlabel("time [s]");
      gnuplot_.setYlabel("flowrate [g/s], pressure [bar]");
      gnuplot_.addDataSetToPlot(x, y, "inflow at top", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y2, "pressure at top", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y3, "cumulated mass", "axes x1y2 w l");
      gnuplot_.addDataSetToPlot(x, y4, "temperature at bottom", "axes x1y2 w l");
      gnuplot_.addDataSetToPlot(x, y5, "outflow at bottom", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y6, "cumulated energy", "axes x1y2 w l");
      gnuplot_.addDataSetToPlot(x, y7, "temperature at top", "axes x1y2 w l");
      gnuplot_.addDataSetToPlot(x, y8, "temperature middle", "axes x1y2 w l");
      gnuplot_.addDataSetToPlot(x, y9, "cumulated energy at bottom", "axes x1y2 w l");
      gnuplot_.addDataSetToPlot(x, y10, "temperature bottom-middle", "axes x1y2 w l");
      gnuplot_.addDataSetToPlot(x, y11, "pressure at bottom", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y12, "losses Seiten", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y13, "losses Top", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y14, "losses Bottom", "axes x1y1 w l");
      // second axis
      gnuplot_.setOption("set y2label \"temperature [°C], mass [Mg], energy [GW]\"");
      std::ostringstream stream;
      stream << "set y2range [" << 0 << ":" << y2Max+1 << "]";
      gnuplot_.setOption(stream.str());
      gnuplot_.setOption("set ytics nomirror");
      gnuplot_.setOption("set y2tics");

      gnuplot_.plot("inflow_plot");
    }


private:
    // the internal method for the initial condition
    void initial_(PrimaryVariables &priVars,
                  const GlobalPosition &globalPos) const
    {
        priVars[pressureIdx] = pressureLow_; // initial condition for the pressure
        priVars[temperatureIdx] = temperatureLow_;
    }

    Scalar temperatureHigh_;
    Scalar temperatureLow_;
    Scalar pressureHigh_;
    Scalar pressureLow_;
    Scalar pressure2_;
    Scalar darcyVelocity_;
    Scalar Zufluss_;
    Scalar Abfluss_;
    const Scalar eps_;
    std::string name_;
    int outputInterval_;
    Dumux::GnuplotInterface<double> gnuplot_;
};

} //end namespace
#endif // DUMUX_1PNI_CONVECTION_PROBLEM_HH
